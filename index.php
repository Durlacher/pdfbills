<?php 
require 'vendor/autoload.php';
use Bill\Bill;
$filelist = glob('public/pdf/*.pdf');
// require('src/bill_functions.php');
$billObj = new Bill();
$bills = $billObj->get_bills_db();
?>
<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <title>PDF Rechnungen</title>
</head>

<body>
    <div class="container">
        <div class="py-5 text-center">
            <h2>PDF ist Chef</h2>
            <p class="lead">Hier finden Sie all Ihre Rechnungen.</p>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Rechnungsnummer</th>
                        <th>Firma</th>
                        <th>Dateiname</th>
                        <th>Datum</th>
                        <th>Bezahlt</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $num = 0;
                        foreach($bills as $bill){
                            $bezahlt = ($bill['bezahlt'] == 1) ? 'Ja':'<input type="checkbox" name="paid" class="paid" value="'.$bill['id'].'">';
                            echo '<tr>';
                            echo '<td>'.$num.'</td>';
                            echo '<td>'.$bill['rnr'].'</td>';
                            echo '<td>'.$bill['firma'].'</td>';
                            echo '<td>'.$bill['filename'].'</td>';
                            echo '<td>'.$bill['date'].'</td>';
                            echo '<td>'. $bezahlt.'</td>';
                            $num++;
                            echo '</tr>';
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="container">
        <div class="py-5 text-center">
            <h2>Rechnungserstellungsformular</h2>
            <p class="lead">Um dem Kunden eine Rechnung zu erstellen, verwenden Sie bitte unten angeführtes Formular.</p>
        </div>

        <div class="row">
     
            <div class="col-md-8 order-md-1">
                <h4 class="mb-3">Neue Rechnung</h4>
                <form class="needs-validation"  id="speicher">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="firma">Firma</label>
                            <input type="text" class="form-control" id="firma" placeholder="" value="" required>
                            <div class="invalid-feedback">
                                Die Firma, an die die Rechnung ausgestellt werden soll.
                            </div>
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="filename">Filename</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="filename" placeholder="Filename" required>
                            <div class="invalid-feedback" style="width: 100%;">
                                Your filename is required.
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 mb-3">
                            <label for="cc-expiration">Rechnungsnummer</label>
                            <input type="text" class="form-control" id="rnr" placeholder="" value="<?php echo $billObj->get_next_billnumber(); ?>" required>
                        </div>
                    </div>
                    <input type="hidden" name="" id="datum" value="<?php  echo date("d.m.Y",time());?>">
                    <hr class="mb-4">
                    <button class="btn btn-primary btn-lg btn-block" type="submit">PDF erzeugen</button>
                </form>
            </div>
        </div>



        <script src="src/bill.js">
        </script>             
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
        </script>
</body>

</html>