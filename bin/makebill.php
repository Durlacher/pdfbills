<?php
require '../vendor/autoload.php';
// reference the Dompdf namespace
use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();
// $dompdf->loadHtmlFile('./template/bill_template.html');
$dompdf->set_option('defaultFont', 'Helvetica');

$filename = dirname(__FILE__).'/template/bill_template.html';
$handle = fopen($filename, "r");
$contents = fread($handle, filesize($filename));
$contents = preg_replace(['/{Rechnungsnummer}/'],['Hund'],$contents);
fclose($handle);
// (Optional) Setup the paper size and orientation
$dompdf->loadHtml($contents);
$dompdf->setPaper('A4', 'landscape');
// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream();
require('../src/bill_functions.php');
$pdf_gen = $dompdf->output();
$bill_filename= 'bill_'.time().'pdf';
if(!file_put_contents('../public/pdf/bill_'.time().'.pdf', $pdf_gen)) {
    echo 'NO';
}else{
    echo 'YES';
    echo insert_bill_db($bill_filename, '', 'R004');
}