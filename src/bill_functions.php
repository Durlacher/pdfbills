<?php

function insert_bill_db($filename, $firma, $billnumber) {
    $host =  'localhost';
    $user = 'root';
    $password = '';
    $dbname = 'bill';
    $dsn = 'mysql:host='. $host .';dbname='. $dbname;

    $pdo = new PDO($dsn, $user, $password);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $sql = 'INSERT INTO bills(rnr, filename,date, firma) VALUES(:rnr, :filename, NOW(), :firma)';
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['rnr' => $billnumber, 'filename' => $filename ,'firma' => $firma]);
}
function get_bills_db() {
    $host =  'localhost';
    $user = 'root';
    $password = '';
    $dbname = 'bill';
    $dsn = 'mysql:host='. $host .';dbname='. $dbname;

    $pdo = new PDO($dsn, $user, $password);
    $sql = 'SELECT * FROM bills';
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $posts = $stmt->fetchAll();
    return $posts;
}
function get_next_billnumber() {
    $host =  'localhost';
    $user = 'root';
    $password = '';
    $dbname = 'bill';
    $dsn = 'mysql:host='. $host .';dbname='. $dbname;

    $pdo = new PDO($dsn, $user, $password);
    $sql = 'SELECT rnr FROM bills ORDER BY rnr DESC LIMIT 1';
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $pose = $stmt->fetchAll();
    $oldnumber = $pose[0][0];
    preg_match ( '/([0-9]+)/', $oldnumber, $matches );
    $number = $matches[1];
    $lastnr = ($number*1)+1;
    return 'R00'.$lastnr;
}

function check_paid($id) {
     $host =  'localhost';
    $user = 'root';
    $password = '';
    $dbname = 'bill';
    $dsn = 'mysql:host='. $host .';dbname='. $dbname;

    $pdo = new PDO($dsn, $user, $password);
    $sql = 'UPDATE bills SET bezahlt = 1 WHERE id = :id';
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['id' => $id]);
}