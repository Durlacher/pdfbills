$(document).ready(function(){ 
    $('#speicher').on('submit', function(e) {
        e.preventDefault();
        let firma = $('#firma').val();
        let rnr = $('#rnr').val();
        let filname = $('#filename').val();
        let datum = $('#datum').val();
        speicherRechnung(rnr,filname,firma,datum);
    });

});

$(document).on("click", '.paid', function(){
    let dings = $(this).val();
    checkPaid(dings);
    
});
function speicherRechnung(rnr,filename,firma,datum) {
    $.ajax({
        url:'http://localhost/Tag24/pdfbills/src/billperpost.php',
        method:'post',
        data: JSON.stringify( {  
           rnr: rnr,
           filename:filename,
           firma:firma,
           datum:datum
        } ),
        success:function(response){
          if(isJson(response)){
                let msg = JSON.parse(response);
                if(msg.ok){
                    console.log('Ok')
                    location.reload();
                }
            }else{
                console.log(response);
            }
        }
    });
}

function checkPaid(id) {
    $.ajax({
        url:'http://localhost/Tag24/pdfbills/src/billperpost.php',
        method:'post',
        data: JSON.stringify( {  
           id: id
        } ),
        success:function(response){
            if(isJson(response)){
                let msg = JSON.parse(response);
                if(msg.ok){
                    console.log('Ok')
                    location.reload();
                }
            }else{
                console.log(response);
            }
        }
    });
}


function isJson(item) {
    item = typeof item !== 'string' ? JSON.stringify(item) : item;

    try {
        item = JSON.parse(item);
    } catch (e) {
        return false;
    }

    if (typeof item === 'object' && item !== null) {
        return true;
    }

    return false;
}
