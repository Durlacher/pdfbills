<?php
require '../vendor/autoload.php';
// require('../src/bill_functions.php');

use Dompdf\Dompdf;
use Bill\Bill;

$bill = new Bill();
$dompdf = new Dompdf();
$dompdf->set_option('defaultFont', 'Helvetica');


$respone = new stdClass;
$respone->ok = false;
$req = json_decode( file_get_contents('php://input') );
if(isset($req->rnr)) {
    $filename = '../bin/template/bill_template.html';
    $handle = fopen($filename, "r");
    $contents = fread($handle, filesize($filename));
    $contents = preg_replace(['/{Rechnungsnummer}/'],[$req->rnr],$contents);
    $contents = preg_replace(['/{Firma}/'],[$req->firma],$contents);
    $contents = preg_replace(['/{Datum}/'],[$req->datum],$contents);
    fclose($handle);
    $dompdf->loadHtml($contents);
    $dompdf->setPaper('A4', 'landscape');
    $dompdf->render();
    $pdf_gen = $dompdf->output();
    $bill_filename= 'bill_'.time().'pdf';
    if(!file_put_contents('../public/pdf/bill_'.$req->filename.time().'.pdf', $pdf_gen)) {
        $respone->ok= false;
        $respone->data = 'Fehler beim speichern';
        echo json_encode(($respone));
    }else{
        $bill->insert_bill_db($req->filename, $req->firma, $req->rnr);
        $respone->ok= true;
        $respone->data = $req;
        echo json_encode($respone);
    } 
}else if(isset($req->id)){
    $bill->check_paid($req->id);
    $respone->ok= true;
    $respone->data = 'Alles ging gut';
    echo json_encode($respone);
}else{
    echo json_encode($respone);
}
