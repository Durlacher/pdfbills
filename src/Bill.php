<?php
namespace Bill;
use \PDO;

class Bill {

    private $host =  'localhost';
    private $user = 'root';
    private $password = '';
    private $dbname = 'bill';
    private $pdo;


    public function __construct() {
        $dsn = 'mysql:host='. $this->host .';dbname='. $this->dbname;
        $this->pdo = new PDO($dsn, $this->user, $this->password);
    }

    public function insert_bill_db($filename, $firma, $billnumber) {
        $sql = 'INSERT INTO bills(rnr, filename,date, firma) VALUES(:rnr, :filename, NOW(), :firma)';
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute(['rnr' => $billnumber, 'filename' => $filename ,'firma' => $firma]);
    }

    public function get_bills_db() {
        $sql = 'SELECT * FROM bills';
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $posts = $stmt->fetchAll();
        return $posts;
    }

    public function get_next_billnumber() {
        $sql = 'SELECT rnr FROM bills ORDER BY rnr DESC LIMIT 1';
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $pose = $stmt->fetchAll();
        $oldnumber = $pose[0][0];
        preg_match ( '/([0-9]+)/', $oldnumber, $matches );
        $number = $matches[1];
        $lastnr = ($number*1)+1;
        return 'R00'.$lastnr;
    }

    function check_paid($id) {
        $sql = 'UPDATE bills SET bezahlt = 1 WHERE id = :id';
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute(['id' => $id]);
    }
}